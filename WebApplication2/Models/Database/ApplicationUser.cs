﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebApplication2.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required, RegularExpression(@"[a-zA-Zа-яА-Я]{3,30}")]
        public string LastName { get; set; }

        [Required, MinLength(4), MaxLength(20)]
        public string ChainShopsName { get; set; }

        public ICollection<Shop> Shops { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

    }
}