﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace WebApplication2.Models
{

    public class Shop
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ShopId { get; set; }

        [Required, StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, MinLength(4), MaxLength(20)]
        public string Type { get; set; }

        [Display(Name = "Sellers count")]
        [Range(1, int.MaxValue)]
        public int SellersCount { get; set; }

        [Required]
        public string Address { get; set; }

        [Display(Name = "Goods count")]
        [Range(0, int.MaxValue)]
        public int GoodsCount { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(IsoDateTimeConverter))]
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Create date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserAccountId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}