﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace WebApplication2.Models
{
    public class ShopViewModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ShopId { get; set; }
        [Required, StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, MinLength(4), MaxLength(20)]
        public string Type { get; set; }

        [Range(1, int.MaxValue)]
        public int SellersCount { get; set; }

        [Required]
        public string Address { get; set; }

        [Range(0, int.MaxValue)]
        public int GoodsCount { get; set; }

        [Required]
        public string CreateDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserAccountId { get; set; }

        public string OwnerName { get; set; }
    }

    public class ShopRandomGenerateViewModel
    {
        [Display(Name = "Amount")]
        [Range(0, int.MaxValue)]
        public int amount { get; set; }
    }

    

}