namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class conf : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shops", "UserAccountId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Shops", "UserAccountId");
            AddForeignKey("dbo.Shops", "UserAccountId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shops", "UserAccountId", "dbo.AspNetUsers");
            DropIndex("dbo.Shops", new[] { "UserAccountId" });
            DropColumn("dbo.Shops", "UserAccountId");
        }
    }
}
