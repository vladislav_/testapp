namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class conf2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "ChainShopsName2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "ChainShopsName2", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
