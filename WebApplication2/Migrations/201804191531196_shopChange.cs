namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class shopChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shops", "SellersCount", c => c.Int(nullable: false));
            AddColumn("dbo.Shops", "GoodsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shops", "GoodsCount");
            DropColumn("dbo.Shops", "SellersCount");
        }
    }
}
