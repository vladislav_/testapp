﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            

            routes.MapRoute(
                name: "Default",
                url: "Home/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Shops",
                url: "Shops/{action}/{id}",
                defaults: new { controller = "Shops", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Account",
                url: "Account/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute("Root", string.Empty, new { controller = "Home", action = "Index" });

            routes.MapRoute(
                "404",
                "{*url}",
                new { controller = "Error", action = "Error404" }
            );
            routes.MapRoute(
                "404_2",
                "{id}",
                new { controller = "Error", action = "Error404" }
            ); routes.MapRoute("Error - 404", "NotFound", new { controller = "Error", action = "NotFound" });
            

        }
    }
}
