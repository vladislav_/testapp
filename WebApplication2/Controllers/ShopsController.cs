﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using WebApplication2.Models;
using OfficeOpenXml;

namespace WebApplication2.Controllers
{
    public class ShopsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Authorize]
        public ActionResult CreateMany()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateMany(ShopRandomGenerateViewModel _amount)
        {

            try
            {
                int amount = _amount.amount;
                if (amount < 0)
                    return base.Content("amount less than 0");
                List<Shop> shops = new List<Shop>();
                string id = User.Identity.GetUserId();
                for (var i = 0; i < amount; i++)
                {
                    shops.Add(new Shop()
                    {
                        UserAccountId = id,
                        Address = RandomString(random.Next(6, 15)),
                        CreateDate = DateTime.Now,
                        GoodsCount = random.Next(1, int.MaxValue - 1),
                        Name = RandomString(random.Next(6, 15)),
                        Type = RandomString(random.Next(6, 15)),
                        SellersCount = random.Next(1, int.MaxValue - 1)
                    });
                }
                db.Shop.AddRange(shops);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //TODO: LOG / error page
                return Content("error");
            }
            return RedirectToAction("Index", "Home");
        }


        [Authorize]
        public ActionResult Create()
        {
            return View(new Shop() { CreateDate = DateTime.Now.Date, SellersCount = 1 });
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Shop shop)
        {
            if (ModelState.IsValid)
            {
                shop.UserAccountId = User.Identity.GetUserId();
                db.Shop.Add(shop);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(shop);
        }

        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = db.Shop.AsNoTracking().FirstOrDefault(p => p.ShopId == id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserAccountId = new SelectList(db.Users, "Id", "LastName", shop.UserAccountId);
            return View(shop);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Shop shop)
        {
            string userId = User.Identity.GetUserId();
            Shop _shop = db.Shop.FirstOrDefault(p => p.ShopId == shop.ShopId);
            if (_shop != null && ModelState.IsValid)
            {
                if (shop.UserAccountId.Equals(_shop.UserAccountId) && shop.UserAccountId.Equals(userId))
                {

                    _shop.Name = shop.Name;
                    _shop.SellersCount = shop.SellersCount;
                    _shop.CreateDate = shop.CreateDate;
                    _shop.GoodsCount = shop.GoodsCount;
                    _shop.Type = shop.Type;
                    _shop.Address = shop.Address;

                    db.Entry(_shop).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(shop);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    Shop shop = db.Shop.Find(id);
                    if (shop != null || shop.UserAccountId.Equals(User.Identity.GetUserId()))
                    {
                        db.Shop.Remove(shop);
                        db.SaveChanges();
                        transaction.Commit();
                        return Json(new { status = "ok" });
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            return Json(new { status = "error" });

        }

        [Authorize]
        public ActionResult LoadFromFile()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult LoadFromFile(HttpPostedFileBase upload)
        {
            try
            {
                if (upload != null)
                {
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    
                    string result = "";

                    //using OfficeOpenXml;
                    using (ExcelPackage xlPackage = new ExcelPackage(upload.InputStream))
                    {
                        var myWorksheet = xlPackage.Workbook.Worksheets.First();
                        var totalRows = myWorksheet.Dimension.End.Row;
                        var totalColumns = myWorksheet.Dimension.End.Column;

                        List<Shop> newShops = new List<Shop>();
                        Shop newShop;
                        ValidationContext context;
                        List<ValidationResult> results;
                        Dictionary<string, int> rows = new Dictionary<string, int>();

                        for (var i = 1; i <= totalColumns; i++)
                        {
                            rows.Add(myWorksheet.Cells[1, i].Text, i);
                        }

                        for (int rowNum = 2; rowNum <= totalRows; rowNum++)
                        {
                            try
                            {
                                newShop = new Shop()
                                {
                                    Name = myWorksheet.Cells[rowNum, rows["Name"]].Text,
                                    Type = myWorksheet.Cells[rowNum, rows["Type"]].Text,
                                    SellersCount = int.Parse(myWorksheet.Cells[rowNum, rows["SellersCount"]].Text),
                                    Address = myWorksheet.Cells[rowNum, rows["Address"]].Text,
                                    GoodsCount = int.Parse(myWorksheet.Cells[rowNum, rows["GoodsCount"]].Text),
                                    CreateDate = DateTime.Parse(myWorksheet.Cells[rowNum, rows["CreateDate"]].Text),
                                    UserAccountId = myWorksheet.Cells[rowNum, rows["UserAccountId"]].Text,
                                };
                                context = new ValidationContext(newShop, null, null);
                                results = new List<ValidationResult>();
                                if (Validator.TryValidateObject(newShop, context, results, true))
                                {
                                    if (UserManager.FindById(newShop.UserAccountId) != null)
                                        newShops.Add(newShop);
                                }
                                else
                                {
                                    //Validation Failed
                                }
                            }
                            catch (Exception ex)
                            {
                                //error firmat
                            }
                        }
                        db.Shop.AddRange(newShops);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //:TODO error page
                return Content("error file data format");
            }
            return RedirectToAction("Index", "Home");
        }

       
        public ActionResult DonutFormatter()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChartData()
        {
            int total = db.Shop.Count();
            if (total <= 0)
            {
                return Json(new {label="no data",value=1, formatted ="0"});
            }
          var result=  db.Shop.ToList().GroupBy(p => p.CreateDate.Date).
                Select(i => new
            {
                label = i.Key.ToString("yy-MM-dd"),
                value = i.Select(p => p.ShopId).Distinct().Count(),
                formatted=Math.Round(((float)i.Select(p => p.ShopId).Distinct().Count() / (float)total)*100f,2)
                }).ToList();
            
            return Json(result);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            View("~\\Views\\Error\\Page404.cshtml").ExecuteResult(this.ControllerContext);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region help

        private Random random = new Random();
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion
    }
}
