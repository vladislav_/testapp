﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using WebApplication2.Helps;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext dbContext;
        private ApplicationUserManager _userManager;

        public HomeController()
        {
            dbContext = new ApplicationDbContext();

        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ActionResult TableJson()
        {
            int pagesize;
            int.TryParse(Request.Form["pagesize"], out pagesize);
            if (pagesize == 0)
                pagesize = 10;
            int page;
            int.TryParse(Request.Form["page"], out page);
            if (page == 0) page = 1;


            string sortcol= Request.Form["sortcol[0]"]??"";
            string sortdir = Request.Form["sortdir[0]"]??"";
            
          
            if (sortdir.Equals("asc"))
                sortdir = "OrderBy";
            else if (sortdir.Equals("desc"))
                sortdir = "OrderByDescending";

            //sort itemd and take for page
            IOrderedQueryable<Shop> items = dbContext.Shop;
            var _items = (typeof(Shop).GetProperty(sortcol) == null ? items.OrderBy(p => p.ShopId) : items.OrderByMethod(sortcol, sortdir)).Skip((page - 1) * pagesize).Take(pagesize); ; ;



            var shops = new List<ShopViewModel>();
            foreach (Shop shop in _items.ToList())
            {
                shops.Add(new ShopViewModel()
                {
                    UserAccountId = shop.UserAccountId,
                    ShopId = shop.ShopId,
                    Name = shop.Name,
                    Address = shop.Address,
                    CreateDate = shop.CreateDate.ToString("MM/dd/yyyy"),
                    GoodsCount = shop.GoodsCount,
                    SellersCount = shop.SellersCount,
                    Type = shop.Type,
                    OwnerName = shop.ApplicationUser.UserName
                });
            }
            int echoId = 0;
            int.TryParse(Request.Form["echo"], out echoId);
            var obj = new { @data = shops, filtered = dbContext.Shop.Count(), echo = echoId };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        //  [ChildActionOnly]
        public ActionResult Table(string AccountId)
        {
            List<Shop> _shops;
            var shops = dbContext.Shop;
            if (string.IsNullOrWhiteSpace(AccountId))
                _shops = shops.ToList();
            else
            {
                _shops = shops.Where(p => p.UserAccountId == AccountId)?.ToList();
            }
            return View(_shops);
        }


        public ActionResult Index()
        {
            var shops = dbContext.Shop;
            var _shops = shops.ToList();
            return View(_shops);
        }



        [Route(@"Home/About/{id}")]
        public ActionResult UserInfo(string id)
        {
            ApplicationUser user = null;
            if (string.IsNullOrWhiteSpace(id) == false)
            {
                user = dbContext.Users.FirstOrDefault(p => p.Id == id);

            }
            else if (Request.IsAuthenticated)
            {
                user = UserManager.FindById(User.Identity.GetUserId());
            }
            if (user != null)
            {
                AccountViewModel viewModel = new AccountViewModel()
                {
                    AccountId = user.Id,
                    ChainShopsName = user.ChainShopsName,
                    LastName = user.LastName,
                    Name = user.UserName,
                    Shops = user.Shops?.ToList()
                };
                return View(viewModel);
            }
            return HttpNotFound();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        protected override void HandleUnknownAction(string actionName)
        {
            View("~\\Views\\Error\\Page404.cshtml").ExecuteResult(this.ControllerContext);
        }
    }
}