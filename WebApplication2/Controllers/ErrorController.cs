﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication2.Controllers
{
    public class ErrorController : Controller
    {

        protected override void HandleUnknownAction(string actionName)
        {
                View("Page404").ExecuteResult(this.ControllerContext);
        }

        public ActionResult Oops()
        {
            return View();
        }
        

        public ActionResult Page404()
        {
            return View();
        }
    }
}